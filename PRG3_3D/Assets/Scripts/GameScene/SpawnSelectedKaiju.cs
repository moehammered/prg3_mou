﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct KaijuPrefab
{
    public GameObject gozu, falsol, rex, kemono, yumKaax, vokrouh;
}

public class SpawnSelectedKaiju : MonoBehaviour {

    public bool childCamera;
    public CharacterSelection selector;
    public KaijuPrefab prefabs;
    public Transform spawnPoint;

    private void spawnKaiju()
    {
        KAIJU_TYPE selectedKaiju = selector.getSelectedKaiju();
        GameObject spawnedKaiju = null;

        switch(selectedKaiju)
        {
            case KAIJU_TYPE.GOZU:
                spawnedKaiju = (GameObject)Instantiate(prefabs.gozu);
                break;

            case KAIJU_TYPE.REX:
                spawnedKaiju = (GameObject)Instantiate(prefabs.rex);
                break;

            case KAIJU_TYPE.FALSOL:
                spawnedKaiju = (GameObject)Instantiate(prefabs.falsol);
                break;

            case KAIJU_TYPE.YUMKAAX:
                spawnedKaiju = (GameObject)Instantiate(prefabs.yumKaax);
                break;

            case KAIJU_TYPE.VOKROUH:
                spawnedKaiju = (GameObject)Instantiate(prefabs.vokrouh);
                break;

            case KAIJU_TYPE.KEMONO:
                spawnedKaiju = (GameObject)Instantiate(prefabs.kemono);
                break;

            default:
                spawnedKaiju = (GameObject)Instantiate(prefabs.gozu);
                break;
        }

        setupKaiju(spawnedKaiju);
    }

    private void setupKaiju(GameObject kaiju)
    {
        KeyboardKaijuController controller = kaiju.GetComponent<KeyboardKaijuController>();
        //controls setup
        controller.controls.walkKey = KeyCode.W;
        controller.controls.leftTurnKey = KeyCode.A;
        controller.controls.rightTurnKey = KeyCode.D;
        controller.controls.specialAttackKey = KeyCode.E;
        controller.controls.primaryAttackKey = KeyCode.Q;
        //setup position
        kaiju.transform.position = spawnPoint.position;
        //setup camera
        if(childCamera)
            Camera.main.transform.parent = kaiju.transform;
    }

	// Use this for initialization
	void Start () {
        spawnKaiju();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
