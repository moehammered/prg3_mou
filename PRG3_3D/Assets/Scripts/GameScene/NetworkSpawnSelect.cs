﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class NetworkSpawnSelect : NetworkBehaviour {

    public bool childCamera;
    public CharacterSelection selector;
    public NetworkManager manager;
    public KaijuPrefab prefabs;
    public Transform spawnPoint;

    private void setSpawnKaiju()
    {
        KAIJU_TYPE selectedKaiju = selector.getSelectedKaiju();

        switch (selectedKaiju)
        {
            case KAIJU_TYPE.GOZU:
                manager.playerPrefab = prefabs.gozu;
                break;

            case KAIJU_TYPE.REX:
                manager.playerPrefab = prefabs.rex;
                break;

            case KAIJU_TYPE.FALSOL:
                manager.playerPrefab = prefabs.falsol;
                break;

            case KAIJU_TYPE.YUMKAAX:
                manager.playerPrefab = prefabs.yumKaax;
                break;

            case KAIJU_TYPE.VOKROUH:
                manager.playerPrefab = prefabs.vokrouh;
                break;

            case KAIJU_TYPE.KEMONO:
                manager.playerPrefab = prefabs.kemono;
                break;

            default:
                manager.playerPrefab = prefabs.gozu;
                break;
        }

        setupKaiju(manager.playerPrefab);
    }

    private void setupKaiju(GameObject kaiju)
    {
        KeyboardKaijuController controller = kaiju.GetComponent<KeyboardKaijuController>();
        //controls setup
        controller.controls.walkKey = KeyCode.W;
        controller.controls.leftTurnKey = KeyCode.A;
        controller.controls.rightTurnKey = KeyCode.D;
        controller.controls.specialAttackKey = KeyCode.E;
        controller.controls.primaryAttackKey = KeyCode.Q;
        //setup position
        kaiju.transform.position = spawnPoint.position;
        //setup camera
        if (childCamera)
            Camera.main.transform.parent = kaiju.transform;
    }

    // Use this for initialization
    void Start()
    {
        setSpawnKaiju();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
