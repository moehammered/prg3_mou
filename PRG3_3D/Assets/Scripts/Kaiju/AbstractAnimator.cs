﻿using UnityEngine;
using System.Collections;

public abstract class AbstractAnimator : MonoBehaviour {

    public Animator anim;

	protected void setAnimParameter(string paramName, bool state)
    {
        anim.SetBool(paramName, state);
    }

    protected void setTimedAnimParameter(string animParam, float seconds, bool state)
    {
        StartCoroutine( startTimedAnim(animParam, seconds, state) );
    }

    private IEnumerator startTimedAnim(string animParam, float seconds, bool state)
    {
        setAnimParameter(animParam, state);
        //wait
        float time = seconds;
        while (time > 0)
        {
            time -= Time.deltaTime;
            yield return null; //wait for a frame
        }
        //flip the state back
        setAnimParameter(animParam, !state);
    }
}
