﻿using UnityEngine;
using System.Collections;

public class DamageableEntity : MonoBehaviour {

    public float health;
    public bool isDead;
    public ModifyHealthDelegate onModifyHealth;
    public delegate void ModifyHealthDelegate();

    protected virtual void Start()
    {
        if(health <= 0)
        {
            isDead = true;
        }
    }

	public void modifyHealth(float amount)
    {
        health += amount;
        if(onModifyHealth != null)
        {
            onModifyHealth();
        }
        else
        {
            Debug.LogWarning(this + ": OnModifyHealth Delegate is not assigned!");
        }
        //we need a callback function to run when an entity takes damage
        if(health <= 0)
        {
            kill();
        }
    }

    public void kill()
    {
        health = 0;
        isDead = true;
        //callback function to run when an entity dies!
    }
}
