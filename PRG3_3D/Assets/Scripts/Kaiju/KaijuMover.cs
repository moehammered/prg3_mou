﻿using UnityEngine;
using System.Collections;

public class KaijuMover : AbstractMover {

    private void debugControls()
    {
        if(Input.GetKey(KeyCode.W))
        {
            walk(1f);
        }
        if(Input.GetKey(KeyCode.A)) //left turn
        {
            turn(-1, 1);
        }
        else if(Input.GetKey(KeyCode.D)) //right turn
        {
            turn(1, 1);
        }
    }

    public void walk()
    {
        move(transform.forward, speed);
    }

    public void walk(float multiplier) //increase movement speed
    {
        move(transform.forward, speed * multiplier);
    }

    public void turn(int direction)
    {
        transform.Rotate(transform.up, direction * speed * Time.deltaTime);
    }

    public void turn(int direction, float multiplier)
    {
        transform.Rotate(transform.up, direction * speed * multiplier * Time.deltaTime);
    }

	// Update is called once per frame
	void FixedUpdate () {
        //debugControls();
	}
}
