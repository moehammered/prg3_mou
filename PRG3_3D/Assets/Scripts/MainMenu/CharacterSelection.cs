﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum KAIJU_TYPE
{
    NONE,
    GOZU,
    FALSOL,
    YUMKAAX,
    VOKROUH,
    KEMONO,
    REX
}

public class CharacterSelection : MonoBehaviour {

    public KAIJU_TYPE selectedKaiju;
    public KaijuPreviewer previewer;
    private const string selectedKaijuKey = "Kaiju.Selected";

    public KAIJU_TYPE getSelectedKaiju()
    {
        string savedSelection = PlayerPrefs.GetString(selectedKaijuKey, "NONE");

        return figureOutKaiju(savedSelection);
    }

    public void selectKaiju(string name)
    {
        selectedKaiju = figureOutKaiju(name);
        previewer.previewKaiju(selectedKaiju);
        saveSelection(selectedKaiju);
    }

    private void saveSelection(KAIJU_TYPE kaiju)
    {
        string kaijuName = getKaijuName(kaiju);
        PlayerPrefs.SetString(selectedKaijuKey, kaijuName);
    }

    public KAIJU_TYPE figureOutKaiju(string name)
    {
        switch( name.ToLower() )
        {
            case "gozu":
                return KAIJU_TYPE.GOZU;

            case "rex":
                return KAIJU_TYPE.REX;

            case "yumkaax":
                return KAIJU_TYPE.YUMKAAX;

            case "falsol":
                return KAIJU_TYPE.FALSOL;

            case "vokrouh":
                return KAIJU_TYPE.VOKROUH;

            case "kemono":
                return KAIJU_TYPE.KEMONO;

            default:
                Debug.LogError(this + ": Unknown Name Given!");
                return KAIJU_TYPE.NONE;
        }
    }

    public string getKaijuName(KAIJU_TYPE kaiju)
    {
        switch (kaiju)
        {
            case KAIJU_TYPE.GOZU:
                return "gozu";

            case KAIJU_TYPE.REX:
                return "rex";

            case KAIJU_TYPE.YUMKAAX:
                return "yumkaax";

            case KAIJU_TYPE.FALSOL:
                return "falsol";

            case KAIJU_TYPE.KEMONO:
                return "kemono";

            case KAIJU_TYPE.VOKROUH:
                return "vokrouh";

            default:
                return "NONE";
        }
    }

	// Use this for initialization
	void Awake () {
        selectedKaiju = getSelectedKaiju();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
