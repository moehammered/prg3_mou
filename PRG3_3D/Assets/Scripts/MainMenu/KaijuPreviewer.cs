﻿using UnityEngine;
using System.Collections;

public class KaijuPreviewer : MonoBehaviour {

    public GameObject gozu, falsol, yumKaax, vokrouh, rex, kemono;

    public void previewKaiju(KAIJU_TYPE kaiju)
    {
        hideAllKaijus();
        switch(kaiju)
        {
            case KAIJU_TYPE.GOZU:
                gozu.SetActive(true);
                break;

            case KAIJU_TYPE.REX:
                rex.SetActive(true);
                break;

            case KAIJU_TYPE.YUMKAAX:
                yumKaax.SetActive(true);
                break;

            case KAIJU_TYPE.FALSOL:
                falsol.SetActive(true);
                break;

            case KAIJU_TYPE.KEMONO:
                kemono.SetActive(true);
                break;

            case KAIJU_TYPE.VOKROUH:
                vokrouh.SetActive(true);
                break;
        }
    }

    public void hideAllKaijus()
    {
        gozu.SetActive(false);
        falsol.SetActive(false);
        yumKaax.SetActive(false);
        vokrouh.SetActive(false);
        rex.SetActive(false);
        kemono.SetActive(false);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
