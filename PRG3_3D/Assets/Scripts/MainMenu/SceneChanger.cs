﻿using UnityEngine;
using System.Collections;

public class SceneChanger : MonoBehaviour {

    public string nextScene;

    public void changeScene()
    {
        Application.LoadLevel(nextScene);
    }

    public void changeScene(string name)
    {
        Application.LoadLevel(name);
    }

    public void restartScene()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    public void closeGame()
    {
        Application.Quit();
    }

}
