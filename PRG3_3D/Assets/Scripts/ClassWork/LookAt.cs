﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class LookAt : MonoBehaviour {

	public Transform target; //Object to look at

	// Update is called once per frame
	void Update () {
		gameObject.transform.LookAt(target);
	}
}
