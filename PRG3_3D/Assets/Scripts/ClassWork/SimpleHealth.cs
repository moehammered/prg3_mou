﻿using UnityEngine;
using System.Collections;

public class SimpleHealth : MonoBehaviour {

    public float health;

    public void takeDamage(float amount)
    {
        health -= amount;
    }
}
