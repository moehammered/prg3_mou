﻿using UnityEngine;
using System.Collections;

public class PhysicsBouncer : MonoBehaviour {

    public Rigidbody targetBody;
    public float forceAmount;

    public void setProperties()
    {
        forceAmount = 15f;
    }

    public void applyForce()
    {
        StartCoroutine(timedForce());
    }

    public void freezeRigidbody()
    {
        StopAllCoroutines();
        targetBody.isKinematic = true;
    }

    private IEnumerator timedForce()
    {
        targetBody.AddForce(Vector3.up * forceAmount, ForceMode.Impulse);
        yield return new WaitForSeconds(2);
        StartCoroutine(timedForce());
    }
}
