﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode] //Makes the Start and Update functions run inside the editor
public class CollisionApplier : MonoBehaviour {

	//this boolean is going to work as a makeshift button in the inspector
	public bool applyColliders = false;
	public bool removeColliders = false;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(applyColliders) //has the boolen been toggled?
		{
			applyColliders = false;
			addCollidersToChildren();
		}
		if(removeColliders)
		{
			removeColliders = false;
			removeCollidersInChildren();
		}
	}
	
	private void addCollidersToChildren()
	{
		MeshFilter[] childrenObjects = gameObject.GetComponentsInChildren<MeshFilter>();
		
		for( int i = 0; i < childrenObjects.Length; i++ )
		{
			childrenObjects[i].gameObject.AddComponent<BoxCollider>();
		}
	}
	
	private void removeCollidersInChildren()
	{
		Collider[] childrenColliders = gameObject.GetComponentsInChildren<Collider>();
		
		for( int i = 0; i < childrenColliders.Length; i++ )
		{
			//this would work but in the editor you must use DestroyImmediate instead of Destroy
			//Destroy(childrenColliders[i]);
			DestroyImmediate(childrenColliders[i]);
		}
	}
}








