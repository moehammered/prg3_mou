﻿using UnityEngine;
using System.Collections;

public class MeshPractice : MonoBehaviour {

	public Vector3[] vertices; //Define the points of the mesh
	public Vector2[] uvs; //Define the mapping points of the vertices
	public int[] triangles; //Defines the points to use to make a triangle

	// Use this for initialization
	void Start () {
		//Construct a mesh
		Mesh mesh = new Mesh(); //Create a new mesh to store our mesh data into
		mesh.vertices = vertices; //Assign the vertices
		mesh.triangles = triangles; //Assign the triangles
		mesh.uv = uvs; //Assign the uvs
		mesh.RecalculateNormals(); //Automatically create normals
		
		//Give the mesh to the renderer
		MeshFilter filter = GetComponent<MeshFilter>(); //Get the mesh filter 
		filter.mesh = mesh; //Supply our mesh to the MeshFilter
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}








