﻿using UnityEngine;
using System.Collections;

public class MoveTowardLerp : MonoBehaviour {

    public Transform target;
    public float timer;
    public float duration; //How long should it interpolate for
    public Vector3 startPoint;
    public Vector3 endPoint;

    private void getPoints()
    {
        startPoint = transform.position;
        endPoint = target.position;
        timer = 0;
    }

    private void lerpPosition()
    {
        timer += Time.deltaTime / duration; //Add the amount of time that has passed this frame
        transform.position = Vector3.Lerp(startPoint, endPoint, timer);
    }

	// Use this for initialization
	void Start () {
        getPoints();
	}
	
	// Update is called once per frame
	void Update () {
        lerpPosition();
	}
}
