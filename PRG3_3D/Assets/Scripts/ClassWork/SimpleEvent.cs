﻿using UnityEngine;
using System.Collections;

public class SimpleEvent : MonoBehaviour {

    public GameObject[] objects;
    public Color colour = Color.yellow;

    public void startEvent()
    {
        //When it happens, perform anything that needs to take affect first before the event begins
        print("Event starting!");
        runEvent(); //trigger the what happens, now that the event has started
    }

    private void runEvent()
    {
        //the actual task/action the event performs and is responsible to do
        foreach(GameObject go in objects)
        {
            go.GetComponent<Renderer>().material.color = colour;
        }
        //now that it has reached this point it is over, so it should end the event
        endEvent();
    }

    public void endEvent()
    {
        //the event is ending, so perform anything that is required to wrap up the event or cleanup
        //after itself if it needs to
        print("Event is finished!");
    }

    //the event responsible for starting our event
	void OnTriggerEnter(Collider col)
    {
        //when this event happens, it needs to 'start' our event up.
        startEvent();
    }
}
