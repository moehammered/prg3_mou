﻿using UnityEngine;
using System.Collections;

public class ManualEventActivator : MonoBehaviour {

    public TimedSimpleEvent timedEvent;
    public SimpleEvent simpleEvent;
    public TimedScaleEvent scaleEvent;

    private void checkKeys()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            //simpleEvent should have its event start
            simpleEvent.startEvent();
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            //timedEvent should have its event start
            timedEvent.startEvent();
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            //timedEvent should have its event start
            scaleEvent.endEvent();
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        checkKeys();
	}
}
