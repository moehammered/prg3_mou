﻿using UnityEngine;
using System.Collections;

public class PracticeEvent : MonoBehaviour {

    public bool isFreeze, isColour;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void changeColour(GameObject obj)
    {
        Renderer rend = obj.GetComponent<Renderer>();

        if(rend)
        {
            rend.material.color = Color.red;
        }
    }

    private void freezePlayer(Rigidbody body)
    {
        body.isKinematic = true;
    }

    void OnTriggerEnter(Collider col)
    {
        print("OnTriggerEnter activated");
        Rigidbody otherBody = col.gameObject.GetComponent<Rigidbody>();

        if(isColour)
        {
            changeColour(col.gameObject);
        }
        else if (isFreeze)
        {
            freezePlayer(otherBody);
        }
        else
        {
            if (otherBody)
            {
                otherBody.AddForce(Vector3.up * 20, ForceMode.Impulse);
            }
        }
    }
}
