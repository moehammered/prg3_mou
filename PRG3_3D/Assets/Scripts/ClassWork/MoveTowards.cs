﻿using UnityEngine;
using System.Collections;

public class MoveTowards : MonoBehaviour {

    public Transform target;
    public Vector3 direction;
    public Vector3 distanceOffset;
    public float speed;
    public float distance;
    public float threatDistance; //The distance it takes for the enemy to attack

    private void checkDistance()
    {
        distanceOffset = target.position - transform.position;
        distance = distanceOffset.magnitude;

        if(distance < threatDistance) //Are we close enough?
        {
            speed = 0; //Stop moving
        }
    }

    private void getDirection()
    {
        direction = target.position - transform.position;
        direction.Normalize(); //This will remove the magnitude from it!!!!
    }

    private void move()
    {
        transform.Translate(direction * speed * Time.deltaTime);
    }

    private void debugLines()
    {
        Debug.DrawLine(transform.position, transform.position + direction, Color.magenta);
    }
	
	// Update is called once per frame
	void Update () {
        checkDistance();
        getDirection();
        debugLines();
        move();
	}
}
