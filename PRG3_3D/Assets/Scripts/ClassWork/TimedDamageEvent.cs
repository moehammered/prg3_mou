﻿using UnityEngine;
using System.Collections;

public class TimedDamageEvent : MonoBehaviour {

    public SimpleHealth player;
    public Color originalColour;

	public void startEvent()
    {
        originalColour = player.gameObject.GetComponent<Renderer>().material.color;
        runEvent();
    }

    private void runEvent()
    {
        StartCoroutine(dealDamage());
    }

    public void endEvent()
    {
        StopAllCoroutines();
        player.GetComponent<Renderer>().material.color = originalColour;
    }

    private IEnumerator dealDamage()
    {
        player.GetComponent<Renderer>().material.color = Color.red;
        player.takeDamage(12);
     
        yield return new WaitForSeconds(0.5f);
        if (player.health > 0)
            StartCoroutine(dealDamage());
        else
            endEvent();
    }

    void OnTriggerEnter(Collider col)
    {
        startEvent();
    }

    void OnTriggerExit(Collider col)
    {
        endEvent();
    }
}
