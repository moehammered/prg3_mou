﻿using UnityEngine;
using System.Collections;

public class BasicEvent : MonoBehaviour {

    public delegate void EventDelegate();
    public EventDelegate onStartEvent, onRunEvent, onEndEvent;

	public void startEvent()
    {
        if(onStartEvent != null) //is there anything to do?
        {
            print("Starting Basic Event System");
            //Do anything that is required to happen before the event begins
            onStartEvent();
        }

        runEvent();
    }

    private void runEvent()
    {
        if(onRunEvent != null)
        {
            onRunEvent();
        }
    }

    public void endEvent()
    {
        if(onEndEvent != null)
        {
            onEndEvent();
        }
    }
}
