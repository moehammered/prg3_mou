﻿using UnityEngine;
using System.Collections;

public class SimpleWorldEvent : MonoBehaviour {

    public TimedDamageEvent damageEvent;
    public TimedScaleEvent scaleEvent;
    public TimedSimpleEvent timedEvent;
    public SimpleEvent simpleEvent;

    public void startEvent()
    {
        damageEvent.startEvent();
        scaleEvent.startEvent();
        timedEvent.startEvent();
        simpleEvent.startEvent();
    }

    private void runEvent()
    {

    }

    public void endEvent()
    {
        damageEvent.endEvent();
        scaleEvent.endEvent();
        timedEvent.endEvent();
        simpleEvent.endEvent();
    }

	void OnTriggerEnter(Collider col)
    {
        startEvent();
    }

    void OnTriggerExit(Collider col)
    {
        endEvent();
    }
}
