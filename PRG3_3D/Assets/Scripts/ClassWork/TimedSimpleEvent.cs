﻿using UnityEngine;
using System.Collections;

public class TimedSimpleEvent : MonoBehaviour {

    public Color originalColour = Color.white;
    public GameObject target;
    public float timer = 0f;

    public void startEvent()
    {
        originalColour = target.GetComponent<Renderer>().material.color;
        timer = 3;
        //run the event now
        runEvent();
    }

    private void runEvent()
    {
        //activate a timed function here
        StartCoroutine(changeColour());
    }

    public void endEvent()
    {
        //restore the original colour 
        target.GetComponent<Renderer>().material.color = originalColour;
        //reset the timer back to default value (0)
        timer = 0;
    }

    private IEnumerator changeColour()
    {
        while(timer > 0)
        {
            timer -= Time.deltaTime;
            yield return null; //wait a frame
            target.GetComponent<Renderer>().material.color = new Color(Random.value, Random.value, Random.value);
        }

        //end the event
        endEvent();
    }

    void OnTriggerEnter(Collider col)
    {
        startEvent();
    }
}
