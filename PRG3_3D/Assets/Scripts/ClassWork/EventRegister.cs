﻿using UnityEngine;
using System.Collections;

public class EventRegister : MonoBehaviour {

    public TimedDamageEvent damageEvent;
    public ParticleSpawner partSpawner;
    public PhysicsBouncer bouncer;
    public BasicEvent eventSystem;

	// Use this for initialization
	void Start () {
        eventSystem.onStartEvent = partSpawner.setSpawnProperties;
        eventSystem.onStartEvent += bouncer.setProperties;

        eventSystem.onRunEvent = partSpawner.spawnParticles;
        eventSystem.onRunEvent += bouncer.applyForce;

        eventSystem.onEndEvent = partSpawner.destroyParticles;
        eventSystem.onEndEvent += bouncer.freezeRigidbody;
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKeyDown(KeyCode.Return))
        {
            eventSystem.startEvent();
        }
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            eventSystem.endEvent();
        }
	}
}
