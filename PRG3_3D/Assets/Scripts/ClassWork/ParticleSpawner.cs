﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParticleSpawner : MonoBehaviour {

    public GameObject particlePrefab;
    public List<GameObject> spawnedObjects;
    public int amount = 1;
    public Vector3 position = Vector3.zero;

    public void setSpawnProperties()
    {
        amount = 5;
        position = new Vector3(0, 2, 0);
        spawnedObjects = new List<GameObject>(amount);
    }

    public void spawnParticles()
    {
        for (int i = 0; i < amount; i++)
        {
            GameObject part = (GameObject)Instantiate(particlePrefab);
            spawnedObjects.Add(part);
        }
    }

    public void destroyParticles()
    {
        foreach(GameObject ob in spawnedObjects)
        {
            Destroy(ob);
        }
    }
}
