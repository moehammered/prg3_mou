﻿using UnityEngine;
using System.Collections;

public class FollowTarget : MonoBehaviour {

    public Transform target;
    public float distance;
    private Vector3 targetOldPosition;

    private Vector3 calculateOffsetPosition()
    {
        Vector3 offset = target.position - targetOldPosition;
        //offset = Vector3.ClampMagnitude(offset, distance);

        return offset;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        
        Vector3 offset = calculateOffsetPosition();
        //if(offset.sqrMagnitude > distance * distance)
        transform.position += offset;

        targetOldPosition = target.position;

        
	}

    void LateUpdate()
    {
        transform.LookAt(target);
    }
}
