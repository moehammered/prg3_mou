﻿using UnityEngine;
using System.Collections;

public class TimedScaleEvent : MonoBehaviour {

    public GameObject target;

    private IEnumerator changeScale()
    {
        while(target.transform.localScale.x > 0.2f)
        {
            Vector3 newScale = target.transform.localScale;

            newScale.x -= 0.4f * Time.deltaTime;
            newScale.y -= 0.4f * Time.deltaTime;
            newScale.z -= 0.4f * Time.deltaTime;

            target.transform.localScale = newScale;

            yield return null;
        }

        endEvent();
    }

    public void startEvent()
    {
        print("Start event now");
        runEvent();
    }

    private void runEvent()
    {
        StartCoroutine(changeScale());
    }

    public void endEvent()
    {
        print("End event");
        StopAllCoroutines();
        StopCoroutine(changeScale());
    }

	void OnTriggerEnter(Collider col)
    {
        startEvent();
    }
}
