﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct Tile
{
    public GameObject tileObject;
    public TILE_TYPE type;
}

public enum DIRECTION
{
    NONE,
    LEFT,
    RIGHT,
    UP,
    DOWN
};

public enum TILE_TYPE
{
    EMPTY,
    HORIZONTAL,     // '=' Shape
    VERTICAL,       // '|' Shape
    BOTTOM_LEFT,    // 'L' Shape
    BOTTOM_RIGHT,   // 'L' Shape Mirrored/Flipped Horizontally
    TOP_RIGHT,      // 'L' Shape 180 Degrees
    TOP_LEFT,       // 'L' Shape Upside down/Flipped Vertically
    END_OF_ENUM
};

public class TileBrancher : MonoBehaviour {

    public Vector2 startPoint;
    public TILE_TYPE startingType;
    public GameObject tilePrefab, horizontalPrefab;
    public Tile[,] grid; //2 dimensional grid of 'tiles' '[,]' means 2D array
    public int gridX = 2, gridZ = 2; //size of map

    private void initialiseGridArray()
    {
        //creates the array and sets its size
        grid = new Tile[gridX, gridZ];

        //initialise the values inside the array!
        for (int z = 0; z < gridZ; z++)
        {
            for (int x = 0; x < gridX; x++)
            {
                grid[x, z].tileObject = null; //Make the object reference null
                grid[x, z].type = TILE_TYPE.EMPTY; //Make every tile empty at first
            }
        }
    }

    private void generateGridObjects()
    {
        for (int z = 0; z < gridZ; z++)
        {
            for (int x = 0; x < gridX; x++)
            {
                //at the current point in the grid, create the tile gameObject
                //that is appropriate to it based on the type at this point in
                //the grid.
                grid[x, z].tileObject = createTileObject(x, z, grid[x, z].type);
            }
        }
    }

    private GameObject createTileObject(int x, int z, TILE_TYPE type)
    {
        //'(GameObject)' is the same as 'as GameObject'
        //this asks the computer to convert the object into a 'GameObject'
        GameObject createdTile = (GameObject)Instantiate(tilePrefab);
        
        //modify it based on the 'type'
        Renderer tileRend = createdTile.GetComponent<Renderer>();
        switch( type ) //check what type the tile is
        {
            case TILE_TYPE.EMPTY: //if the type is of 'EMPTY'
                //colour the tile BLACK if so
                if(tileRend) //if it is not null
                {
                    tileRend.material.color = Color.black;
                }
                break;

            case TILE_TYPE.HORIZONTAL:
                createdTile = (GameObject)Instantiate(horizontalPrefab);
                if(tileRend)
                {
                    tileRend.material.color = Color.red;
                }
                break;

            case TILE_TYPE.VERTICAL:
                if(tileRend)
                {
                    tileRend.material.color = Color.yellow;
                }
                break;

            case TILE_TYPE.BOTTOM_LEFT:
                if (tileRend)
                {
                    tileRend.material.color = Color.blue;
                }
                break;

            case TILE_TYPE.BOTTOM_RIGHT:
                if (tileRend)
                {
                    tileRend.material.color = Color.cyan;
                }
                break;

            case TILE_TYPE.TOP_RIGHT:
                if (tileRend)
                {
                    tileRend.material.color = Color.green;
                }
                break;

            case TILE_TYPE.TOP_LEFT:
                if (tileRend)
                {
                    tileRend.material.color = Color.magenta;
                }
                break;
        }

        Vector3 tilePosition = Vector3.zero;

        //Get the correct size of the tile
        SquareBuilder tileSettings = tilePrefab.GetComponent<SquareBuilder>();
        if (tileSettings) //check if it is not null
        {
            //create the appropriate position
            tilePosition.x = x * tileSettings.size;
            tilePosition.z = z * tileSettings.size;
        }
        //position it
        createdTile.transform.position = tilePosition;

        return createdTile; //return the tile we have created out of this function
    }

    private void startStepping()
    {
        int startX = (int)startPoint.x; //convert the start point into an int
        int startZ = (int)startPoint.y;

        //setup the first tile that we start on
        grid[startX, startZ].type = startingType;
        //run the function to step into the 'starting' point
        stepThroughGrid(startX, startZ, startingType);
    }

    private void stepThroughGrid(int x, int z, TILE_TYPE type)
    {
        //get the available directions!
        DIRECTION[] availableDirections = getAvailableDirections(type);
        //Take a step in each direction (eventually)!
        for (int i = 0; i < availableDirections.Length; i++)
        {
            switch( availableDirections[i] ) //check our current direction
            {
                case DIRECTION.NONE:
                    //don't move anywhere!
                    break;

                case DIRECTION.UP:
                    if(z+1 < gridZ)
                    {
                        if(grid[x, z+1].type == TILE_TYPE.EMPTY)
                        {
                            grid[x, z + 1].type = getRandomTileType(availableDirections[i]);
                            //step onto the tile
                            z++;
                            //step through the grid
                            stepThroughGrid(x, z, grid[x, z].type);
                            //step back once it is done travelling
                            z--;
                        }
                    }
                    break;

                case DIRECTION.DOWN:
                    if(z-1 >= 0)
                    {
                        if(grid[x, z-1].type == TILE_TYPE.EMPTY)
                        {
                            grid[x, z - 1].type = getRandomTileType(availableDirections[i]);
                            z--;
                            stepThroughGrid(x, z, grid[x, z].type);
                            z++;
                        }
                    }
                    break;

                case DIRECTION.LEFT:
                    //check if moving left is within the grid!
                    if( x-1 >= 0 )
                    {
                        //check to see if the tile on the left is EMPTY
                        if(grid[x-1, z].type == TILE_TYPE.EMPTY)
                        {
                            //create a tile here!
                            grid[x - 1, z].type = getRandomTileType(availableDirections[i]);
                            //step onto that tile!
                            x--;
                            //continue stepping through the grid!
                            stepThroughGrid(x, z, grid[x, z].type);
                            //take a step back once we're done travelling
                            x++;
                        }
                    }
                    break;

                case DIRECTION.RIGHT:
                    //check if moving right is within the grid!
                    if( x+1 < gridX )
                    {
                        //check to see if the tile on the right is EMPTY
                        if(grid[x+1,z].type == TILE_TYPE.EMPTY)
                        {
                            //create a tile here!
                            grid[x + 1, z].type = getRandomTileType(availableDirections[i]);
                            //step onto that tile!
                            x++;
                            //Continue stepping through the grid!
                            stepThroughGrid(x, z, grid[x, z].type);
                            //step back once we are done travelling
                            x--;
                        }
                    }
                    break;
            }
        }
        //When taking a step, it must first check if it is within the grid!
        //Then it needs to check if it is stepping on an empty tile
        //If it is on an empty tile, it needs to make a tile there!
        //It needs to step onto the tile it just created!
    }

    private DIRECTION[] getAvailableDirections(TILE_TYPE type)
    {
        DIRECTION[] directions = new DIRECTION[1]; 

        switch(type) //check to see what type it is
        {
            case TILE_TYPE.EMPTY:
                directions[0] = DIRECTION.NONE; //Don't move
                break;

            case TILE_TYPE.HORIZONTAL:
                directions = new DIRECTION[2]; //Fit 2 directions
                directions[0] = DIRECTION.LEFT;
                directions[1] = DIRECTION.RIGHT;
                break;

            case TILE_TYPE.VERTICAL:
                directions = new DIRECTION[2]; //Fit 2 directions
                directions[0] = DIRECTION.UP;
                directions[1] = DIRECTION.DOWN;
                break;

            case TILE_TYPE.BOTTOM_LEFT:
                directions = new DIRECTION[2]; //Fit 2 directions
                directions[0] = DIRECTION.UP;
                directions[1] = DIRECTION.RIGHT;
                break;

            case TILE_TYPE.BOTTOM_RIGHT:
                directions = new DIRECTION[2]; //Fit 2 directions
                directions[0] = DIRECTION.UP;
                directions[1] = DIRECTION.LEFT;
                break;

            case TILE_TYPE.TOP_RIGHT:
                directions = new DIRECTION[2]; //Fit 2 directions
                directions[0] = DIRECTION.LEFT;
                directions[1] = DIRECTION.DOWN;
                break;

            case TILE_TYPE.TOP_LEFT:
                directions = new DIRECTION[2]; //Fit 2 directions
                directions[0] = DIRECTION.RIGHT;
                directions[1] = DIRECTION.DOWN;
                break;

            default:
                directions[0] = DIRECTION.NONE;
                break;
        }

        return directions; //return the directions we've found
    }

    private TILE_TYPE getRandomTileType()
    {
        int randomNumber = Random.Range(1, (int)TILE_TYPE.END_OF_ENUM);

        //convert randomNumber into a TILE_TYPE and return it
        return (TILE_TYPE)randomNumber;
    }

    private TILE_TYPE getRandomTileType(DIRECTION dir)
    {
        //a function that will return an array of suitable TILE_TYPE to choose from!
        TILE_TYPE[] suitableTiles = getSuitableTiles(dir);
        //make the randomNumber be a random value between 0 - length of array
        //return the tileType from the array
        int randomNumber = Random.Range(0, suitableTiles.Length);

        //convert randomNumber into a TILE_TYPE and return it
        return suitableTiles[randomNumber];
    }

    private TILE_TYPE[] getSuitableTiles(DIRECTION dir)
    {
        TILE_TYPE[] types = new TILE_TYPE[1];

        switch( dir ) //check the direction we supply
        {
            case DIRECTION.LEFT:
                types = new TILE_TYPE[3]; //We have 3 tiles with the suitable direction
                types[0] = TILE_TYPE.HORIZONTAL;
                types[1] = TILE_TYPE.BOTTOM_LEFT;
                types[2] = TILE_TYPE.TOP_LEFT;
                break;

            case DIRECTION.RIGHT:
                types = new TILE_TYPE[3]; //We have 3 tiles with the suitable direction
                types[0] = TILE_TYPE.HORIZONTAL;
                types[1] = TILE_TYPE.TOP_RIGHT;
                types[2] = TILE_TYPE.BOTTOM_RIGHT;
                break;

            case DIRECTION.DOWN:
                types = new TILE_TYPE[3];
                types[0] = TILE_TYPE.VERTICAL;
                types[1] = TILE_TYPE.BOTTOM_LEFT;
                types[2] = TILE_TYPE.BOTTOM_RIGHT;
                break;

            case DIRECTION.UP:
                types = new TILE_TYPE[3];
                types[0] = TILE_TYPE.VERTICAL;
                types[1] = TILE_TYPE.TOP_RIGHT;
                types[2] = TILE_TYPE.TOP_LEFT;
                break;
        }

        return types;
    }

	// Use this for initialization
	void Start () {
        initialiseGridArray(); //needs to run first!
        startStepping();
        generateGridObjects(); //needs to run last!
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
