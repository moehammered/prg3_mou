﻿using UnityEngine;
using System.Collections;

public class LocalCoordinateVisualiser : MonoBehaviour {

    public float localAxisScale;

    private void debugLines()
    {
        //Local Axis Lines
        Debug.DrawLine(transform.position, transform.position + transform.right * localAxisScale, Color.red); //X axis
        Debug.DrawLine(transform.position, transform.position + transform.up * localAxisScale, Color.green); //Y Axis
        Debug.DrawLine(transform.position, transform.position + transform.forward * localAxisScale, Color.blue); //Z axis
    }

	// Update is called once per frame
	void Update () {
        debugLines();
	}
}
